<?php

namespace App\Form\StarWars\Person;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ShowPersonType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['attr' => ['readonly' => true]])
            ->add('gender', TextType::class, ['attr' => ['readonly' => true]])
            ->add('birthYear', TextType::class, ['attr' => ['readonly' => true]])
            ->add('eyeColor', TextType::class, ['attr' => ['readonly' => true]])
            ->add('skinColor', TextType::class, ['attr' => ['readonly' => true]])
            ->add('height', NumberType::class, ['attr' => ['readonly' => true]])
            ->add('mass', NumberType::class, ['attr' => ['readonly' => true]]);
    }
}