<?php

namespace App\Controller;

use App\Form\StarWars\Person\ShowPersonType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\StarWars\Film;
use App\Entity\StarWars\StarWars;

class StarWarsController extends AbstractController
{
    /**
     * @Route("/", name="star_wars.index")
     */
    public function index()
    {
        $starWars = new StarWars();

        try {
        /**
         * @var Film[] $films
         */
        $films = $starWars->getFilms();
        } catch (\Exception $e) {
            $films = [];
        }

        return $this->render(
            'controller/star_wars/index.html.twig',
            ['films' => $films]
        );
    }

    /**
     * @Route("/star_wars/show_person/{name}", name="star_wars.show_person")
     * @param string $name
     */
    public function showPerson($name)
    {
        $starWars = new StarWars();

        try {
            $person = $starWars->getPersonByName($name);
        } catch (\Exception $e) {
            $person = null;
        }

        if (null === $person) {
            return $this->render('controller/star_wars/show_person.html.twig', [
                'person_form' => null
            ]);
        }

        $form = $this->createForm(ShowPersonType::class, $person);

        return $this->render('controller/star_wars/show_person.html.twig', [
            'person_form' => $form->createView(),
            'films_acted' => $person->getFilms()
        ]);
    }
}