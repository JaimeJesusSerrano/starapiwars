<?php

namespace App\Entity\StarWars;

use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Entity\Swapi\CollectionConsumer as SwapiCollectionConsumer;

class StarWars
{
    const API_SITE = 'https://swapi.co/api/';
    const API_FILMS = 'films/';
    const API_PEOPLE = 'people/';

    /**
     * @var FilesystemCache
     */
    private $cache;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var Film[]
     */
    private $films;

    /**
     * @var Person[]
     */
    private $people;

    public function __construct()
    {
        $this->cache = new FilesystemCache();

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);

//        $this->cache->clear();
//        $this->films = null;
//        $this->people = null;
    }

    /**
     * @return Film[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getFilms()
    {
        if (null === $this->films) {
            if (!$this->cache->has('star_wars.films')) {
                $this->films = $this->_getFilms();
                $this->cache->set('star_wars.films', $this->films);

                $people = $this->getPeople();
                $this->_populatePeopleIntoFilms($this->films, $people);
                $this->cache->set('star_wars.films', $this->films);
            } else {
                $this->films = $this->cache->get('star_wars.films');
            }
        }

        return $this->films;
    }

    /**
     * @return Film[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function _getFilms()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', self::API_SITE . self::API_FILMS);
        $body = $response->getBody();

        /**
         * @var SwapiCollectionConsumer $swapiConsumer
         */
        $swapiConsumer = $this->serializer->deserialize($body, SwapiCollectionConsumer::class, 'json');
        $filmsArray = $swapiConsumer->getResults();
        $films = [];
        foreach ($filmsArray as $filmArray) {
            $film = new Film();
            $film->setUrl($filmArray['url']);
            $film->setTitle($filmArray['title']);
            $film->setPeopleUrl($filmArray['characters']);
            $films[] = $film;
        }

        return $films;
    }

    /**
     * @param array $films
     * @param array $people
     */
    protected function _populatePeopleIntoFilms(array $films, array $people) : void
    {
        foreach ($films as $film) {
            $this->_populatePeopleIntoFilm($film, $people);
        }
    }

    /**
     * @param Film $film
     * @param array $people
     */
    protected function _populatePeopleIntoFilm(Film $film, array $people) : void
    {
        /** @var Film $film */
        foreach ($people as $person) {
            if (in_array($person->getUrl(), $film->getPeopleUrl())) {
                $film->addPerson($person);
            }
        }
    }

    /**
     * @return Person[]|mixed|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getPeople()
    {
        if (null === $this->people) {
            if (!$this->cache->has('star_wars.people')) {
                $this->people = $this->_getPeople();
                $this->cache->set('star_wars.people', $this->people);

                $films = $this->getFilms();
                $this->_populateFilmsIntoPeople($this->people, $films);
                $this->cache->set('star_wars.people', $this->people);
            } else {
                $this->people = $this->cache->get('star_wars.people');
            }
        }

        return $this->people;
    }

    /**
     * @return Person[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function _getPeople()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', self::API_SITE . self::API_PEOPLE);
        $body = $response->getBody();

        /**
         * @var SwapiCollectionConsumer $swapiConsumer
         */
        $swapiConsumer = $this->serializer->deserialize($body, SwapiCollectionConsumer::class, 'json');
        $peopleArray = $swapiConsumer->getResults();
        $people = [];
        foreach ($peopleArray as $personArray) {
            $person = new Person();
            $person->setUrl($personArray['url']);
            $person->setName($personArray['name']);
            $person->setBirthYear($personArray['birth_year']);
            $person->setEyeColor($personArray['eye_color']);
            $person->setGender($personArray['gender']);
            $person->setHairColor($personArray['hair_color']);
            $person->setHeight($personArray['height']);
            $person->setMass($personArray['mass']);
            $person->setSkinColor($personArray['skin_color']);
            $person->setFilmsUrl($personArray['films']);
            $people[] = $person;
        }

        return $people;
    }

    /**
     * @param array $people
     * @param array $films
     */
    protected function _populateFilmsIntoPeople(array $people, array $films) : void
    {
        foreach ($people as $person) {
            $this->_populateFilmsIntoPerson($person, $films);
        }
    }

    /**
     * @param Person $person
     * @param array $films
     */
    protected function _populateFilmsIntoPerson(Person $person, array $films) : void
    {
        /** @var Film $film */
        foreach ($films as $film) {
            if (in_array($film->getUrl(), $person->getFilmsUrl())) {
                $person->addFilm($film);
            }
        }
    }

    /**
     * @param string $name
     * @return Person|null
     */
    public function getPersonByName($name) : ?Person
    {
        if (empty($name)) {
            return null;
        }

        $people = $this->getPeople();

        foreach ($people as $person) {
            if ($person->getName() === $name) {
                return $person;
            }
        }

        return null;
    }
}