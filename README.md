# Starapiwars
# ===========
Example project to consume the Star Wars Api (https://swapi.co/) and show the data.

## Technologies
- Symfony 4
- Doctrine
- Bootstrap 4

## Steps to run it
- Git clone git clone ``` $ git clone https://bitbucket.org/JaimeJesusSerrano/starapiwars.git ```
- Go to project root folder  ``` $ cd .../starapiwars/ ```
- Composer install ``` $ composer install ``` and solve all installation dependencies
- Ensure yarn program is installed (``` $ npm install --global yarn ```)
- Yarn install ``` $ yarn install ``` ``` $ yarn add @symfony/webpack-encore --dev ```
- Yarn compilation ``` $ yarn encore dev ```
- Finally... start server ``` $ php bin/console server:start ```